#ifndef menu_h
#define menu_h

#include <iostream>
#include "timer.h"
#include "generator.h"
#include "array.h"
#include "list.h"
#include "binary_heap.h"
#include "red_black_tree.h"
#include <limits>


using namespace std;

extern int liczba_danych;

void first_choice();
void array_menu(string data_file_path);
void list_menu(string data_file_path);
void heap_menu(string data_file_path);
void tree_menu(string data_file_path);

#endif