#include "stdafx.h"
#include "menu.h"


void first_choice()								// menu pierwszego wyboru - struktury
{
	int choice;
	int data_count;
	int counter = 0;
	string data_file_path = "generated_data.txt"; //standardowa siezka do pliku

	do
	{	
		char generate_data_file;

		if (counter==0)
		{
			counter++;
			cout << "Chcesz, zeby program wygenerowal plik z danymi? (Y/N)    ";
			cin >> generate_data_file;
			if (generate_data_file == 'Y' || generate_data_file == 'y')
			{
				cout << "\nPodaj ile danych ma byc wygenerowanych \n"
					"do pracy ze strukturami: ";
				do
				{
					cin.clear();
					cin.ignore();
				} while (!(cin >> data_count));
				//funkcja generatora danych
				generate_data(data_count);
			}
			else
			{
				cout << "Wprowadz sciezke do pliku z danymi: ";
				cin >> data_file_path;
			}
		}

		system("cls");
		//wybor struktury na jakiej bedziemy pracowac 
		cout << "Wybierz jedna z opcji:\n"
			"1. Operacje na tablicy\n"
			"2. Operacje na liscie\n"
			"3. Operacje na kopcu binarnym\n"
			"4. Wyjscie\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			array_menu(data_file_path);
			break;
		case 2:
			list_menu(data_file_path);
			break;
		case 3:
			heap_menu(data_file_path);
			break;
		case 4:
			cout << "Wyjscie...\n";
			break;
		}
	} while (choice != 4);
	
}	   

void array_menu(string data_file_path)
{//menu dla tablicy
	int * array = load_array_from_file(data_file_path);
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	double tm;
	int* result;
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do tablicy\n"
			"2. Usun z tablicy\n"
			"3. Wyszukaj w tablicy\n"
			"4. Wyswietl tablice\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			char random;
			cout << "Chcesz wybrac losowy index do dodania? (Y/N)      ";
			cin >> random;
			if (random=='Y' || random=='y')
			{
				index = (rand() % array[0]) + 1;
			}
			else
			{
				cout << "Podaj numer indeksu tablicy pod jaki chcesz dodac wartosc: <1," << array[0] + 1 << ">\n";
				do
				{
					cin.clear();
					cin.ignore();
				} while (!(cin >> index));
			}

			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			//start pomiaru czasu
			performanceCountStart = start_timer();
			array = add_to_array(array, index, value); //wywolanie funkcji dodawania, przypisanie wyniku
			performanceCountEnd = end_timer();
			tm = (performanceCountEnd.QuadPart - performanceCountStart.QuadPart);
			cout << endl << "Time: " << tm  << endl << endl;
			display_array(array);
			cout << endl;

			break;
		case 2:
			cout << "Chcesz wybrac losowy index do usuniecia? (Y/N)      ";
			cin >> random;
			if (random == 'Y' || random == 'y')
			{
				index = (rand() % array[0]-1)+1;
			}
			else
			{
				cout << "Podaj numer indeksu tablicy do usuniecia: <1," << array[0] << ">\n";
				do
				{
					cin.clear();
					cin.ignore();
				} while (!(cin >> index));
			}
			performanceCountStart = start_timer();
			array = delete_from_array(array, index);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl << endl;
			display_array(array);
			cout << endl;
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			performanceCountStart = start_timer();
			result = search_in_array(array, value);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			//wyswietlenie, na jakich pozycjach wystepuje szukana wartosc
			if (result[0] == 0 )
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: "<< endl;
				for (int i = 1; i <= result[0]; i++)
				{
					cout << result[i] << endl;
				}
				cout << endl;
			}

			delete[] result;
			break;
		case 4: //wyswietlenie tablicy w 10 kolumnach
			display_array(array);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
	
}

void list_menu(string data_file_path)
{
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	double tm;
	list_element * first = load_list_from_file(data_file_path);
	int * results=new int[];
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do listy\n"
			"2. Usun z listy\n"
			"3. Wyszukaj w liscie\n"
			"4. Wyswietl liste\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			char random;
			cout << "Chcesz wygenerwowac losowy index do dodania? (Y/N)      ";
			cin >> random;
			if (random == 'Y' || random == 'y')
			{
				index = (rand() % first->value) + 1;
			}
			else
			{
				cout << "Podaj numer indeksu listy pod jaki chcesz dodac wartosc: <1," << first->value + 1 << ">\n";
				do
				{
					cin.clear();
					cin.ignore();
				} while (!(cin >> index));
			}

			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			performanceCountStart = start_timer();
			add_to_list(first, index-1, value);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			display_list(first);
			cout << endl;
			break;
		case 2:
			cout << "Chcesz wybrac losowy index do usuniecia? (Y/N)      ";
			cin >> random;
			if (random == 'Y' || random == 'y')
			{
				index = (rand() % first->value - 1) + 1;
			}
			else
			{
				cout << "Podaj numer indeksu listy do usuniecia: <1," << first->value << ">\n";
				do
				{
					cin.clear();
					cin.ignore();
				} while (!(cin >> index));
			}
			performanceCountStart = start_timer();
			delete_from_list(first, index-1);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			display_list(first);
			cout << endl;
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			performanceCountStart = start_timer();
			results = search_in_list(first, value);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;

			if (results[0] == 0)
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: " << endl;
				for (int i = 1; i <= results[0]; i++)
				{
					cout << results[i] << endl;
				}
				cout << endl;
			}

			break;
		case 4:
			display_list(first);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
	delete[] results;
}

void heap_menu(string data_file_path)
{
	int * heap = load_heap_from_file(data_file_path);
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	double tm = 0;
	int choice = 0;
	int* result;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do kopca\n"
			"2. Usun z kopca\n"
			"3. Wyszukaj w kopcu\n"
			"4. Wyswietl kopiec\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj wartosc do dodania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			performanceCountStart = start_timer();
			heap = add_to_heap(heap, value);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			display_heap(heap);
			cout << endl;
			break;
		case 2:
			cout << "Podaj numer indeksu kopca do usuniecia: <1," << heap[0] << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));

			performanceCountStart = start_timer();
			heap = delete_from_heap(heap, index);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			display_heap(heap);
			cout << endl;
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			performanceCountStart = start_timer();
			result = search_in_heap(heap, value);
			performanceCountEnd = end_timer();
			tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
			cout << endl << "Time: " << tm << endl;
			if (result[0] == 0)
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: " << endl;
				for (int i = 1; i <= result[0]; i++)
				{
					cout << result[i] << endl;
				}
				cout << endl;
			}

			delete[] result;
			break;
		case 4:
			display_heap(heap);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
	delete[] heap;
}

void tree_menu(string data_file_path)
{
	int * tree = load_array_from_file(data_file_path);
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do tablicy\n"
			"2. Usun z tablicy\n"
			"3. Wyszukaj w tablicy\n"
			"4. Wyswietl tablice\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj numer indeksu drzewa pod jaki chcesz dodac wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));
			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));
			add_to_tree(tree, index, value);
			break;
		case 2:
			cout << "Podaj index elemntu do usuniecia: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));
			delete_from_tree(tree, index);
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));
			search_in_tree(tree, value);
			break;
		case 4:
			display_tree(tree);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
}
