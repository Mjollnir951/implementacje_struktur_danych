#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

struct list_element
{
	list_element* head;
	list_element* tail;
	int value;

	list_element();
	~list_element();
};


void add_to_list(list_element * first, int index, int value);
void delete_from_list(list_element * first, int index);
int * search_in_list(list_element * first, int value);
void display_list(list_element * first);
list_element * load_list_from_file(string source);

