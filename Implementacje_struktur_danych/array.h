#ifndef array_h
#define array_h

#include <iostream>
#include <fstream>
#include <iomanip>
#include "menu.h"

using namespace std;

int* add_to_array(int* array, int index, int value);
int* delete_from_array(int* array, int index);
int* search_in_array(int* array, int value);
void display_array(int* array);
int* load_array_from_file(string source);

#endif