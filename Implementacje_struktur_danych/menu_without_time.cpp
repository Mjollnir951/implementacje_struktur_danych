#include "stdafx.h"
#include "menu_without_time.h"


void first_choice_without_time()						// menu pierwszego wyboru - struktury
{
	int choice;
	int data_count;
	int counter = 0;
	string data_file_path = "generated_data.txt";

	do
	{
		char generate_data_file;

		if (counter == 0)
		{
			counter++;
			cout << "Chcesz, zeby program wygenerowal plik z danymi? Y/N\n";
			cin >> generate_data_file;
			if (generate_data_file == 'Y' || generate_data_file == 'y')
			{
				cout << "\nPodaj ile danych ma byc wygenerowanych \n"
					"do pracy ze strukturami: ";
				cin >> data_count;

				generate_data(data_count);
			}
			else
			{
				cout << "Wprowadz sciezke do pliku z danymi: ";
				cin >> data_file_path;
			}
		}

		system("cls");

		cout << "Wybierz jedna z opcji:\n"
			"1. Operacje na tablicy\n"
			"2. Operacje na liscie\n"
			"3. Operacje na kopcu binarnym\n"
			"4. Wyjscie\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			array_menu_without_time(data_file_path);
			break;
		case 2:
			list_menu_without_time(data_file_path);
			break;
		case 3:
			heap_menu_without_time(data_file_path);
			break;
		case 4:
			cout << "Wyjscie...\n";
			break;
		}
	} while (choice != 4);

}

void array_menu_without_time(string data_file_path)
{
	int * array = load_array_from_file(data_file_path);
	int* result;
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do tablicy\n"
			"2. Usun z tablicy\n"
			"3. Wyszukaj w tablicy\n"
			"4. Wyswietl tablice\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj numer indeksu tablicy pod jaki chcesz dodac wartosc: <1," << array[0] + 1 << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));

			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			array = add_to_array(array, index, value);
			display_array(array);
			cout << endl;

			break;
		case 2:
			cout << "Podaj numer indeksu tablicy do usuniecia: <1," << array[0] << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));

			array = delete_from_array(array, index);
			display_array(array);
			cout << endl;

			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			result = search_in_array(array, value);

			if (result[0] == 0)
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: " << endl;
				for (int i = 1; i <= result[0]; i++)
				{
					cout << result[i] << endl;
				}
				cout << endl;
			}

			delete[] result;
			break;
		case 4:
			display_array(array);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);

}

void list_menu_without_time(string data_file_path)
{
	list_element * first = load_list_from_file(data_file_path);
	int * results;
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do listy\n"
			"2. Usun z listy\n"
			"3. Wyszukaj w liscie\n"
			"4. Wyswietl liste\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj numer indeksu listy pod jaki chcesz dodac wartosc: <1," << first->value + 1 << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));
			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			add_to_list(first, index-1, value);
			display_list(first);
			cout << endl;
			break;
		case 2:
			cout << "Podaj numer indeksu listy do usuniecia: <1," << first->value << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));

			delete_from_list(first, index-1);
			display_list(first);
			cout << endl;
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			results = search_in_list(first, value);

			if (results[0] == 0)
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: " << endl;
				for (int i = 1; i <= results[0]; i++)
				{
					cout << results[i] << endl;
				}
				cout << endl;
			}

			delete[] results;
			break;
		case 4:
			display_list(first);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
}

void heap_menu_without_time(string data_file_path)
{
	int * heap = load_heap_from_file(data_file_path);
	int choice = 0;
	int* result;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do kopca\n"
			"2. Usun z kopca\n"
			"3. Wyszukaj w kopcu\n"
			"4. Wyswietl kopiec\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj wartosc do dodania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			heap = add_to_heap(heap, value);
			display_heap(heap);
			cout << endl;
			break;
		case 2:
			cout << "Podaj numer indeksu kopca do usuniecia: <1," << heap[0] << ">\n";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));

			heap = delete_from_heap(heap, index);
			display_heap(heap);
			cout << endl;
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));

			result = search_in_heap(heap, value);
			if (result[0] == 0)
			{
				cout << "Nie udalo sie znalezc takiego elementu.\n" << endl;
			}
			else
			{
				cout << "Mam! Element/y, ktorego/ych szukasz ma indeks/y: " << endl;
				for (int i = 1; i <= result[0]; i++)
				{
					cout << result[i] << endl;
				}
				cout << endl;
			}

			delete[] result;
			break;
		case 4:
			display_heap(heap);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
	delete[] heap;
}

void tree_menu_without_time(string data_file_path)
{
	int * tree = load_array_from_file(data_file_path);
	int choice;

	do
	{
		cout << "Wybierz opcje:\n"
			"1. Dodaj do tablicy\n"
			"2. Usun z tablicy\n"
			"3. Wyszukaj w tablicy\n"
			"4. Wyswietl tablice\n"
			"5. Powrot\n";
		cin >> choice;

		switch (choice)
		{
		case 1:
			int index, value;
			cout << "Podaj numer indeksu drzewa pod jaki chcesz dodac wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));
			cout << "Podaj wartosc: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));
			add_to_tree(tree, index, value);
			break;
		case 2:
			cout << "Podaj index elemntu do usuniecia: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> index));
			delete_from_tree(tree, index);
			break;
		case 3:
			cout << "Podaj wartosc do wyszukania: ";
			do
			{
				cin.clear();
				cin.ignore();
			} while (!(cin >> value));
			search_in_tree(tree, value);
			break;
		case 4:
			display_tree(tree);
			break;
		case 5:
			cout << "Powrot...";
			break;
		}
	} while (choice != 5);
}
