#ifndef menu_without_time_h
#define menu_without_time_h

#include <iostream>
#include "timer.h"
#include "generator.h"
#include "array.h"
#include "list.h"
#include "binary_heap.h"
#include "red_black_tree.h"
#include <limits>


using namespace std;

extern int liczba_danych;

void first_choice_without_time();
void array_menu_without_time(string data_file_path);
void list_menu_without_time(string data_file_path);
void heap_menu_without_time(string data_file_path);
void tree_menu_without_time(string data_file_path);

#endif