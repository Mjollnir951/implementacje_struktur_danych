#include "stdafx.h"
#include "list.h"

list_element::list_element()
{

}

list_element::~list_element()
{

}

void add_to_list(list_element * first, int index, int value)
{
	list_element * tmp = new list_element;
	list_element * current = new list_element;

	if (index == 0)
	{
		if (first->value == 0)
		{
			first->head = tmp;
			first->tail = tmp;
			tmp->head = NULL;
			tmp->tail = NULL;
			tmp->value = value;
		}
		else
		{
			first->head->tail = tmp;
			tmp->head = first->head;
			first->head = tmp;
			tmp->value = value;
		}
	}
	else if (first->value == index)
	{
		tmp->tail = first->tail;
		first->tail->head = tmp;
		first->tail = tmp;
		tmp->head = NULL;
		tmp->value = value;
		
	}
	else
	{														
		current = first;
		if (first->value/2 >= index)
		{
			for (int i = 1; i < index+1; i++)
			{
				current = current->head;
			}

			current->head->tail = tmp;
			tmp->head = current->head;
			current->head = tmp;
			tmp->tail = current;
			tmp->value = value;
			
		}
		else
		{
			for (int i = 1; i < (first->value-index+2); i++)
			{
				current = current->tail;
			}
				current->head->tail = tmp;
				tmp->head = current->head;
				current->head = tmp;
				tmp->tail = current;
				tmp->value = value;
			
		}
	}
	
	first->value++;
}

void delete_from_list(list_element * first, int index)
{
	list_element * current;
	current = first;

	if (first->value>1)
	{
		if (index == 0)
		{
			first->head->head->tail = NULL;
			first->head = first->head->head;
		}
		else if (first->value == index+1)
		{
			first->tail->tail->head = NULL;
			first->tail = first->tail->tail;
		}						
		else
		{
			for (int i = 1; i < index+1; i++)
			{
				current = current->head;
			}
			current->tail->head = current->head;
			current->head->tail = current->tail;
		}
	}

	first->value--;
}

int * search_in_list(list_element * first, int value)
{
	list_element * current = new list_element;
	current = first;
	int counter = 0;
	int * results = new int[first->value];

	for (int i = 0; i < first->value-1; i++)
	{
		if (current->value == value)
		{
			counter++;
			results[counter] = i;
		}
		current = current->head;
	}
	results[0] = counter;
	return results;
}

void display_list(list_element * first)
{
	list_element * current = new list_element;
	current = first;
	char from_the_end;
	int counter = 0;

	cout << "Tak wygladaja poszczegolne elementy listy: (" << first->value << " elementy/ow)\n";
	for (int i = 0; i < first->value; i++)
	{
		if (i % 10 == 0)
		{
			cout << endl;
		}
		current = current->head;
		cout << setw(6) << current->value;
	}
	cout << endl;
	cout << "Wyswietlic od konca? (Y/N)       ";
	cin >> from_the_end;
	if (from_the_end == 'Y' || from_the_end=='y')
	{
		for (int i = first->value; i > 0; i--)
		{
			if (counter % 10 == 0)
			{
				cout << endl;
			}
			cout << setw(6) << current->value;
			current = current->tail;
			counter++;
		}
	}
	cout << endl;
	cin.get();
}

list_element * load_list_from_file(string source)
{
	list_element * first = new list_element; // stworzenie HEAD'a listy, ktory w value trzyma jej rozmiar

	int size;
	int tmp;
	fstream file;
	file.open(source , ios::in);

	if (file.good())
	{
		file >> size;
		first->head = NULL;
		first->tail = NULL;
		first->value = 0;

		for (int i = 0; i < size; i++)
		{
			file >> tmp;
			add_to_list(first, i , tmp );
		}
		file.close();
	}
	return first;
}
