#ifndef red_black_tree_h
#define red_black_tree_h

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

struct red_black_tree
{

};

int * add_to_tree(int * tree, int index, int value);
int * delete_from_tree(int * tree, int index);
int * search_in_tree(int * tree, int value);
void display_tree(int * tree);
int * load_tree_from_file(string source);


#endif