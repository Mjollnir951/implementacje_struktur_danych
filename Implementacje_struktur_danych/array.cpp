#include "stdafx.h"
#include "array.h"


int* add_to_array(int* array, int index, int value)
{
	array[0]++;
	int * result = new int[array[0] + 1];

	for (int i = 0; i < index; i++)
	{
		result[i] = array[i];
	}
	result[index] = value;
	for (int i = index+1; i < array[0]+1; i++)
	{
		result[i] = array[i - 1];
	}

	return result;
}

int* delete_from_array(int* array, int index)
{
	int * result = new int[array[0]];
	array[0]--;

	for (int i = 0; i < index; i++)
	{
		result[i] = array[i];
	}
	for (int i = index; i < array[0]+1; i++)
	{
		result[i] = array[i + 1];
	}
	return result;
}

int* search_in_array(int* array, int value)
{
	int counter=0;
	int* result = new int[array[0]];
	for (int i = 1; i < array[0]+1; i++)
	{
		if (array[i]==value)
		{
			counter++;
			result[counter] = i;
		}
		
	}
	result[0] = counter;
	return result;
}

void display_array(int* array)
{
	cout << "Tak wygladaja poszczegolne elementy tablicy: (" << array[0] << " elementy/ow)\n";
	for (int i = 1; i < array[0]+1; i++)
	{
		if (i%10==1)
		{
			cout << endl;
		}
		cout << setw(6) << array[i];
	}
	cout <<endl;
	cin.get();
}

int* load_array_from_file(string source)
{
	int size;
	int tmp;
	fstream file;
	file.open(source, ios::in);

	if (file.good())
	{
		file >> size;
		int * array = new int[1];
		array[0] = 0;

		for (int i = 1; i < size+1; i++)
		{
			file >> tmp;
			array = add_to_array(array, i, tmp);
		}
		file.close();
		return array;
	}
	else
	{
		cout << "Problem z dostepem do pliku danych!\n";
		return NULL;
	}
	
}