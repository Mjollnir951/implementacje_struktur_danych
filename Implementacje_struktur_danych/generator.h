#ifndef generator_h
#define generator_h

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <fstream>

using namespace std;

void generate_data(int ilosc);
void save_data(int* data, int ilosc);

#endif