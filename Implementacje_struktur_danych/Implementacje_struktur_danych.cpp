// Implementacje_struktur_danych.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "menu.h"
#include "menu_without_time.h"
#include "generator.h"
#include "array.h"
#include "list.h"
#include "binary_heap.h"
#include "red_black_tree.h"
#include <iostream>


int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	char with_time;
	
	cout << "Witaj w programie!\n";
	cout << "Czy chcesz uruchomic wersje z pomiarem czasu? (Y/N)    ";
	cin >> with_time;
	if (with_time=='Y' || with_time=='y')	// od tej decyzji zalezy z ktorego menu skorzystamy
	{
		first_choice();
	}
	else
	{
		first_choice_without_time();
	}
	
	
	return 0;
}

