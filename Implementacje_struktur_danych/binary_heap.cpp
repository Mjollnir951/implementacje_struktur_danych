#include "stdafx.h"
#include "binary_heap.h"

int * add_to_heap(int * heap, int value)
{
	heap[0]++;
	
	int * result = new int[heap[0]+1];
	for (int i = 0; i < heap[0]; i++)
	{
		result[i] = heap[i];
	}

	delete[] heap;
	result[result[0]] = value;

	while (!is_heap_in_order(result))
	{
		result = fix_heap(result);
	}

	return result;
}

int * fix_heap(int * heap)
{
	int tmp;
	for (int i = heap[0]; i > 0; i--)
	{
		if (2 * i > heap[0])
		{
			continue;
		}
		else if (2 * i + 1 > heap[0])
		{
			if (heap[2 * i] > heap[i])
			{
				tmp = heap[i];
				heap[i] = heap[2 * i];
				heap[2 * i] = tmp;
			}
		}
		else
		{
			if (heap[2 * i]>heap[i] || heap[2 * i + 1]>heap[i])
			{
				if (heap[2*i]>heap[2*i+1])
				{
					tmp = heap[i];
					heap[i] = heap[2 * i];
					heap[2 * i] = tmp;
				}
				else
				{
					tmp = heap[i];
					heap[i] = heap[2 * i+1];
					heap[2 * i+1] = tmp;
				}
			}
		}
	}
	return heap;
}

bool is_heap_in_order(int * heap)
{
	int i = heap[0];
	for (i ; i > 0; i--)
	{
		if (2 * i > heap[0])
		{
			continue;
		}
		else if (2 * i + 1 > heap[0])
		{
			if (heap[2 * i] > heap[i])
			{
				return false;
			}
		}
		else
		{
			if (heap[2*i]>heap[i] || heap[2*i+1]>heap[i])
			{
				return false;
			}
		}
	}
	return true;
}


int * delete_from_heap(int * heap, int index)
{

	int * result = new int[heap[0]];
	heap[0]--;

	for (int i = 0; i < index; i++)
	{
		result[i] = heap[i];
	}
	for (int i = index; i < heap[0]+1; i++)
	{
		result[i] = heap[i + 1];
	}

	while (!is_heap_in_order(result))
	{
		result = fix_heap(result);
	}
	return result;
}

int * search_in_heap(int * heap, int value)
{
	int counter = 0;
	int * result = new int[heap[0]];
	for (int i = 1; i < heap[0]+1; i++)
	{
		if (heap[i]==value)
		{
			counter++;
			result[counter] = i;
		}
	}
	result[0] = counter;
	return result;
}

void display_heap(int * heap)
{
	int levels = log2(heap[0])+1;
	string space = "   ";
	int temp = 1;
	cout << "Tak wygladaja poszczegolne elementy kopca: (" << heap[0] << " elementy/ow, " << levels << " poziomy/ow)\n";

	if (levels%2==0)
	{
		for (int i = 1; i < heap[0]+1; i++)
		{
			for (int i = 0; i < 2*levels-1; i++)
			{
				cout << space;
			}
			cout << setw(6) << heap[i];
			for (int i = 0; i < 2*levels-1; i++)
			{
				cout << space;
			}
			if (i + 2 == pow(2, temp) + 1)
			{
				temp++;
				cout << endl << endl;
				levels = levels / 2;
			}
		}
	}
	else
	{
		for (int i = 1; i < heap[0] + 1; i++)
		{
			for (int i = 0; i < levels; i++)
			{
				cout << space;
			}
			cout << setw(6) << heap[i];
			for (int i = 0; i < levels; i++)
			{
				cout << space;
			}
			if (i + 2 == pow(2, temp) + 1)
			{
				temp++;
				cout << endl << endl;
				levels = levels / 2;
			}
		}
	}


	cout << endl;
		
}

int * load_heap_from_file(string source)
{
	int size;
	int tmp;
	fstream file;
	file.open(source, ios::in);

	if (file.good())
	{
		file >> size;
		int * heap = new int[1];
		heap[0] = 0;

		for (int i = 1; i < size + 1; i++)
		{
			file >> tmp;
			heap = add_to_heap(heap, tmp);
		}
		file.close();
		return heap;
	}
	else
	{
		cout << "Problem z dostepem do pliku danych!\n";
		return NULL;
	}
}