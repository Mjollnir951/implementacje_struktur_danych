#ifndef binary_heap_h
#define binary_heap_h

#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <string>

using namespace std;

int * add_to_heap(int * heap, int value);
bool is_heap_in_order(int * heap);
int * fix_heap(int * heap);
int * delete_from_heap(int * heap, int value);
int * search_in_heap(int * heap, int value);
void display_heap(int * heap);
int * load_heap_from_file(string source);


#endif