#include "stdafx.h"
#include "generator.h"

void generate_data(int ilosc)			
{ //generujemy zadana ilosc danych 
	int* data = new int[ilosc+1];
	data[0] = ilosc;
	for (int i = 1; i <= ilosc; i++)
	{
		data[i] = (rand() % 200)-100; 
	}
	save_data(data, ilosc); //wywolujemy funkcje zapisu do pliku
}

void save_data(int* data, int ilosc)
{
	fstream file("generated_data.txt", ios::out);
	for (int i = 0; i <= ilosc; i++)
	{
		file << data[i] << endl;
	}

	file.close();
	delete[] data;
}